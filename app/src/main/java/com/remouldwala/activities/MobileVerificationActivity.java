package com.remouldwala.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.remouldwala.R;
import com.remouldwala.custom_components.OnSingleClickListener;

public class MobileVerificationActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private TextView mTvMobileNo;
    private MaterialButton mBtnVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        initView();

        setToolbar();

        mBtnVerify.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(MobileVerificationActivity.this, CreatePasswordActivity.class);
                startActivity(intent);
            }
        });

    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mTvMobileNo = findViewById(R.id.tv_mobile_no);
        mBtnVerify = findViewById(R.id.btn_verify);
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("Verification");
    }
}

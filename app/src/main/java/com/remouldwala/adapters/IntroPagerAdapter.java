package com.remouldwala.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.remouldwala.R;
import com.remouldwala.models.IntroModel;

import java.util.List;

public class IntroPagerAdapter extends PagerAdapter {

    private List<IntroModel> introModels;
    private Context mContext;
    private LayoutInflater layoutInflater;

    public IntroPagerAdapter(List<IntroModel> introModels, Context mContext) {
        this.introModels = introModels;
        this.mContext = mContext;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return introModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        IntroModel introModel = introModels.get(position);

        View itemView = layoutInflater.inflate(R.layout.intro_slider_view, container, false);

        ImageButton mIvThumbnail = itemView.findViewById(R.id.iv_thumbnail);
        TextView mTvTitle = itemView.findViewById(R.id.tv_title);
        TextView mTvDescription = itemView.findViewById(R.id.tv_description);
        LinearLayout mLlMainLayout = itemView.findViewById(R.id.ll_main_layout);

        mLlMainLayout.setBackgroundColor(mContext.getResources().getColor(introModel.getBack_color()));
        Glide.with(mContext).load(introModel.getThumbnail()).diskCacheStrategy(DiskCacheStrategy.ALL).into(mIvThumbnail);
        mTvDescription.setText(introModel.getDescription());
        mTvTitle.setText(introModel.getTitle());

        container.addView(itemView);

        return itemView;
    }

    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}

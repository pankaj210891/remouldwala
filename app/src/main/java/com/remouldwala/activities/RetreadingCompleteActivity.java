package com.remouldwala.activities;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.remouldwala.R;

public class RetreadingCompleteActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private ImageButton mIvAdd;
    private TextView mTxtVehicleNo;
    private TextView mTvVehicleNo;
    private TextView mTxtDriverName;
    private TextView mTvDriverName;
    private TextView mTxtMobileNo;
    private TextView mTvMobileNo;
    private MaterialButton mBtnConfirm;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retreading_complete);
        mContext = this;

        initView();

        setSupportActionBar(mToolbar);
        mTvPageTitle.setText("Tyre Receiving Confirm");
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mIvAdd = findViewById(R.id.iv_add);
        mTxtVehicleNo = findViewById(R.id.txt_vehicle_no);
        mTvVehicleNo = findViewById(R.id.tv_vehicle_no);
        mTxtDriverName = findViewById(R.id.txt_driver_name);
        mTvDriverName = findViewById(R.id.tv_driver_name);
        mTxtMobileNo = findViewById(R.id.txt_mobile_no);
        mTvMobileNo = findViewById(R.id.tv_mobile_no);
        mBtnConfirm = findViewById(R.id.btn_confirm);
    }
}

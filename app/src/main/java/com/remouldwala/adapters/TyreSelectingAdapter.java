package com.remouldwala.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.remouldwala.R;
import com.remouldwala.models.TyreInfoModel;

import java.util.List;

public class TyreSelectingAdapter extends RecyclerView.Adapter<TyreSelectingAdapter.Holder> {

    private List<TyreInfoModel> tyreInfoModels;
    private Context mContext;
    private LayoutInflater layoutInflater;

    public TyreSelectingAdapter(List<TyreInfoModel> tyreInfoModels, Context mContext) {
        this.tyreInfoModels = tyreInfoModels;
        this.mContext = mContext;
        layoutInflater = LayoutInflater.from(mContext);

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(layoutInflater.inflate(R.layout.tyre_info_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        TyreInfoModel infoModel = tyreInfoModels.get(position);

        holder.mTvSelectSize.setText(infoModel.getSize());
        holder.mTvQuantity.setText(infoModel.getQuantity());
        holder.mTvSelectPattern.setText(infoModel.getPattern());

    }

    @Override
    public int getItemCount() {
        return tyreInfoModels.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private TextView mTvSelectSize;
        private TextView mTvQuantity;
        private TextView mTvSelectPattern;

        Holder(@NonNull View itemView) {
            super(itemView);

            mTvSelectSize = itemView.findViewById(R.id.tv_select_size);
            mTvQuantity = itemView.findViewById(R.id.tv_quantity);
            mTvSelectPattern = itemView.findViewById(R.id.tv_select_pattern);
        }
    }
}

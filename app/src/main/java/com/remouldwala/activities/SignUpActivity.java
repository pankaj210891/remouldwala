package com.remouldwala.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.remouldwala.R;
import com.remouldwala.custom_components.OnSingleClickListener;

public class SignUpActivity extends BaseActivity {

    private TextInputLayout mTilName;
    private TextInputEditText mEtName;
    private TextInputLayout mTilMobile;
    private TextInputEditText mEtMobile;
    private TextInputLayout mTilEmail;
    private TextInputEditText mEtEmail;
    private TextInputLayout mTilVehicleNo;
    private TextInputEditText mEtVehicleNo;
    private TextInputLayout mTilAddress;
    private TextInputEditText mEtAddress;
    private TextInputLayout mTilGstNo;
    private TextInputEditText mEtGstNo;
    private LinearLayout mTvHaveAccount;
    private MaterialButton mBtnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initView();


        mTvHaveAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mBtnSignIn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, MobileVerificationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        mTilName = findViewById(R.id.til_name);
        mEtName = findViewById(R.id.et_name);
        mTilMobile = findViewById(R.id.til_mobile);
        mEtMobile = findViewById(R.id.et_mobile);
        mTilEmail = findViewById(R.id.til_email);
        mEtEmail = findViewById(R.id.et_email);
        mTilVehicleNo = findViewById(R.id.til_vehicle_no);
        mEtVehicleNo = findViewById(R.id.et_vehicle_no);
        mTilAddress = findViewById(R.id.til_address);
        mEtAddress = findViewById(R.id.et_address);
        mTilGstNo = findViewById(R.id.til_gst_no);
        mEtGstNo = findViewById(R.id.et_gst_no);
        mTvHaveAccount = findViewById(R.id.tv_have_account);
        mBtnSignIn = findViewById(R.id.btn_sign_in);
    }
}

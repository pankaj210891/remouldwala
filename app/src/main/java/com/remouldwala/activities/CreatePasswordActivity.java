package com.remouldwala.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.remouldwala.R;
import com.remouldwala.custom_components.OnSingleClickListener;

public class CreatePasswordActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private TextInputLayout mTilPassword;
    private TextInputEditText mEtPassword;
    private TextInputLayout mTilConfirmPassword;
    private TextInputEditText mEtConfirmPassword;
    private MaterialButton mBtnSetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_password);
        initView();

        setToolbar();


        mBtnSetPassword.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(CreatePasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("Set Password");
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mTilPassword = findViewById(R.id.til_password);
        mEtPassword = findViewById(R.id.et_password);
        mTilConfirmPassword = findViewById(R.id.til_confirm_password);
        mEtConfirmPassword = findViewById(R.id.et_confirm_password);
        mBtnSetPassword = findViewById(R.id.btn_set_password);
    }
}

package com.remouldwala.models;

public class TimeLineModel {

    private String tv_status;
    private String tv_orderstatus_time;

    public TimeLineModel(String tv_status, String tv_orderstatus_time) {
        this.tv_status = tv_status;
        this.tv_orderstatus_time = tv_orderstatus_time;
    }

    public String getTv_status() {
        return tv_status;
    }

    public void setTv_status(String tv_status) {
        this.tv_status = tv_status;
    }

    public String getTv_orderstatus_time() {
        return tv_orderstatus_time;
    }

    public void setTv_orderstatus_time(String tv_orderstatus_time) {
        this.tv_orderstatus_time = tv_orderstatus_time;
    }
}

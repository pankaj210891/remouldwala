package com.remouldwala.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.remouldwala.R;
import com.remouldwala.models.TimeLineModel;

import java.util.List;

public class TrackingAdapter extends RecyclerView.Adapter<TrackingAdapter.TimeLineViewHolder> {

    private List<TimeLineModel> timeLineModelList;
    private Context mContext;
    private LayoutInflater layoutInflater;
    private boolean isOn = false;

    public TrackingAdapter(List<TimeLineModel> timeLineModelList, Context mContext) {
        this.timeLineModelList = timeLineModelList;
        this.mContext = mContext;

        layoutInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public TimeLineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TimeLineViewHolder(layoutInflater.inflate(R.layout.item_timeline, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TimeLineViewHolder holder, int position) {

        TimeLineModel lineModel = timeLineModelList.get(position);

        holder.mTvStatus.setText(lineModel.getTv_status());
        holder.mTvOrderstatusTime.setText(lineModel.getTv_orderstatus_time());

        if (position == 0) {
            holder.mIvUpperLine.setVisibility(View.INVISIBLE);
        } else if (position == getItemCount() - 1) {
            holder.mIvLowerLine.setVisibility(View.INVISIBLE);
            holder.mLyOrderstatusTime.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return timeLineModelList.size();
    }


    class TimeLineViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIvUpperLine;
        private ImageView mIvCircle;
        private ImageView mIvLowerLine;
        private LinearLayout mLyStatus;
        private TextView mTvStatus;
        private LinearLayout mLyOrderstatusTime;
        private ImageView mImageview;
        private TextView mTvOrderstatusTime;

        TimeLineViewHolder(@NonNull View itemView) {
            super(itemView);

            mIvUpperLine = itemView.findViewById(R.id.iv_upper_line);
            mIvCircle = itemView.findViewById(R.id.iv_circle);
            mIvLowerLine = itemView.findViewById(R.id.iv_lower_line);
            mLyStatus = itemView.findViewById(R.id.ly_status);
            mTvStatus = itemView.findViewById(R.id.tv_status);
            mLyOrderstatusTime = itemView.findViewById(R.id.ly_orderstatus_time);
            mImageview = itemView.findViewById(R.id.imageview);
            mTvOrderstatusTime = itemView.findViewById(R.id.tv_orderstatus_time);
        }
    }
}

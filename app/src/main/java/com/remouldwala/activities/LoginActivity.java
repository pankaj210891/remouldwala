package com.remouldwala.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.remouldwala.R;
import com.remouldwala.custom_components.OnSingleClickListener;

public class LoginActivity extends BaseActivity {

    private TextInputLayout mTilPassword;
    private TextInputEditText mEtPassword;
    private LinearLayout mTvNoAccount;
    private TextView mTvForgotPassword;
    private MaterialButton mBtnSignIn;
    private TextInputLayout mTilMobile;
    private TextInputEditText mEtMobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

        mTvNoAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mTvForgotPassword.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(LoginActivity.this, FeedbackActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        mTilPassword = findViewById(R.id.til_password);
        mEtPassword = findViewById(R.id.et_password);
        mTvNoAccount = findViewById(R.id.tv_no_account);
        mTvForgotPassword = findViewById(R.id.tv_forgot_password);
        mBtnSignIn = findViewById(R.id.btn_sign_in);
        mTilMobile = findViewById(R.id.til_mobile);
        mEtMobile = findViewById(R.id.et_mobile);
    }
}

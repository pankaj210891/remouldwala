package com.remouldwala.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.remouldwala.R;
import com.remouldwala.adapters.TrackingAdapter;
import com.remouldwala.custom_components.OnSingleClickListener;
import com.remouldwala.models.TimeLineModel;

import java.util.ArrayList;

public class TrackingPageActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private ImageButton mIvAdd;
    private LinearLayout mLlBottomLayout;
    private Context mContext;
    private RecyclerView mRvTimeline;
    private TrackingAdapter trackingAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_page);
        mContext = this;

        initView();
        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("Progress");
        mIvBack.setVisibility(View.VISIBLE);

        mIvBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        mRvTimeline.setLayoutManager(linearLayoutManager);
        trackingAdapter = new TrackingAdapter(getStoreDetail(), mContext);
        mRvTimeline.setAdapter(trackingAdapter);
    }

    public ArrayList<TimeLineModel> getStoreDetail() {
        ArrayList<TimeLineModel> status = new ArrayList<>();
        status.add(new TimeLineModel("Retreader has confirm your request", "8:30am,Jan 31,2018"));
        status.add(new TimeLineModel("Vehicle on the way for tyre pickup", "10:30am,Jan 31,2018"));
        status.add(new TimeLineModel("Tyre Inspection", "aaaaaa"));
        status.add(new TimeLineModel("No. of tyres picked up for retreading", "aaaaaa"));
        return status;
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mIvAdd = findViewById(R.id.iv_add);
        mLlBottomLayout = findViewById(R.id.ll_bottom_layout);
        mRvTimeline = findViewById(R.id.rv_timeline);
    }
}

package com.remouldwala.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.remouldwala.R;
import com.remouldwala.custom_components.OnSingleClickListener;
import com.remouldwala.models.TyrePatternModel;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class TyrePatternAdapter extends RecyclerView.Adapter<TyrePatternAdapter.PatternHolder> {

    private List<TyrePatternModel> tyrePatterModels;
    private Context mContext;
    private LayoutInflater layoutInflater;
    private OnPatterSelection onPatterSelection;
    private int selectedIndex = -1;
    private NumberFormat format;

    public TyrePatternAdapter(List<TyrePatternModel> tyrePatterModels, Context mContext, OnPatterSelection onPatterSelection) {
        this.tyrePatterModels = tyrePatterModels;
        this.mContext = mContext;
        layoutInflater = LayoutInflater.from(mContext);
        this.onPatterSelection = onPatterSelection;
        format = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public PatternHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PatternHolder(layoutInflater.inflate(R.layout.pattern_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PatternHolder holder, final int position) {
        final TyrePatternModel patternModel = tyrePatterModels.get(position);

        if (selectedIndex != -1) {
            if (position == selectedIndex) {
                holder.mIvChecked.setVisibility(View.VISIBLE);
            } else {
                holder.mIvChecked.setVisibility(View.GONE);
            }
        } else {
            holder.mIvChecked.setVisibility(View.GONE);
        }

        holder.mIvPatternPrice.setText(format.format(Float.parseFloat(patternModel.getPrice())));

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (selectedIndex != position) {
                    selectedIndex = position;

                    onPatterSelection.onSelectedPattern(patternModel.getPattern(), patternModel.getPrice());

                    notifyDataSetChanged();

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return tyrePatterModels.size();
    }


    class PatternHolder extends RecyclerView.ViewHolder {

        private ImageView mIvPatternImage;
        private ImageView mIvChecked;
        private TextView mIvPatternPrice;

        PatternHolder(@NonNull View itemView) {
            super(itemView);
            mIvPatternImage = itemView.findViewById(R.id.iv_pattern_image);
            mIvChecked = itemView.findViewById(R.id.iv_checked);
            mIvPatternPrice = itemView.findViewById(R.id.iv_pattern_price);
        }
    }


    public interface OnPatterSelection {
        void onSelectedPattern(String selectedPattern, String price);
    }
}

package com.remouldwala.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.remouldwala.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        goToNextScreen();

    }

    private void goToNextScreen() {
        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

                Intent i = new Intent(SplashActivity.this, WhyUsActivity.class);
                startActivity(i);
                finish();

            }

        }, 5 * 1000);
    }
}

package com.remouldwala.models;

public class IntroModel {

    private long id;
    private String title;
    private String description;
    private String thumbnail;
    private int back_color;


    public IntroModel(long id, String title, String description, String thumbnail, int back_color) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.back_color = back_color;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBack_color() {
        return back_color;
    }

    public void setBack_color(int back_color) {
        this.back_color = back_color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}

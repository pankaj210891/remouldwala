package com.remouldwala.utils;

import android.app.AlarmManager;

public class Constants {
    public static long NEW_MONK_API_INTERVAL = AlarmManager.INTERVAL_DAY;
    public static String LOGGER = "newMonk.txt";

    public static String SHARED_PREFERENCE_NAME = "ScreenLoadTimePref";
    public static String SHARED_PREFERENCE_USER_ID = "userId";

    public static String EMPTY_STRING = "";
    public static String BLANK_SPACE = " ";
}

package com.remouldwala.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.remouldwala.R;
import com.remouldwala.models.TyreInfoModel;
import com.remouldwala.utils.ResUtil;
import com.remouldwala.utils.Utilities;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.PaymentHolder> {

    private List<TyreInfoModel> paymentModels;
    private Context mContext;
    private onUpdatePrice onUpdatePrice;
    private LayoutInflater layoutInflater;
    private NumberFormat format;

    public PaymentAdapter(List<TyreInfoModel> paymentModels, Context mContext, onUpdatePrice onUpdatePrice) {
        this.paymentModels = paymentModels;
        this.mContext = mContext;
        this.onUpdatePrice = onUpdatePrice;

        layoutInflater = LayoutInflater.from(mContext);

        format = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public PaymentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PaymentHolder(layoutInflater.inflate(R.layout.payment_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentHolder holder, int position) {
        TyreInfoModel paymentModel = paymentModels.get(position);

        LinearLayout.LayoutParams params0 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        holder.mTvPattern.setLayoutParams(params0);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        holder.mTvTyreSize.setLayoutParams(params);

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        holder.mTvNoOfTyre.setLayoutParams(params1);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        holder.mTvPrice.setLayoutParams(params2);

        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        holder.mTvTotal.setLayoutParams(params3);

        StringBuilder patternBuilder = new StringBuilder();
        patternBuilder.append(paymentModel.getPattern());

        holder.mTvPattern.setText(patternBuilder);

        holder.mTvTyreSize.setText(paymentModel.getSize());
        holder.mTvNoOfTyre.setText(paymentModel.getQuantity());

        StringBuilder priceBuilder = new StringBuilder();
        priceBuilder.append(format.format(Float.parseFloat(paymentModel.getPrice())));

        holder.mTvPrice.setText(priceBuilder);

        StringBuilder totalBuilder = new StringBuilder();
        float price = Float.parseFloat(paymentModel.getQuantity()) * Float.parseFloat(paymentModel.getPrice());
        totalBuilder.append(format.format(price));

        holder.mTvTotal.setText(totalBuilder);

        onUpdatePrice.onFinalPrice(Float.parseFloat(paymentModel.getQuantity()) * Float.parseFloat(paymentModel.getPrice()));

    }

    @Override
    public int getItemCount() {
        return paymentModels.size();
    }


    class PaymentHolder extends RecyclerView.ViewHolder {

        private TextView mTvTyreSize;
        private TextView mTvNoOfTyre;
        private TextView mTvPrice;
        private TextView mTvTotal;
        private TextView mTvPattern;

        PaymentHolder(@NonNull View itemView) {
            super(itemView);
            mTvTyreSize = itemView.findViewById(R.id.tv_tyre_size);
            mTvNoOfTyre = itemView.findViewById(R.id.tv_no_of_tyre);
            mTvPrice = itemView.findViewById(R.id.tv_price);
            mTvTotal = itemView.findViewById(R.id.tv_total);
            mTvPattern = itemView.findViewById(R.id.tv_pattern);
        }
    }

    public interface onUpdatePrice {

        void onFinalPrice(float price);
    }
}

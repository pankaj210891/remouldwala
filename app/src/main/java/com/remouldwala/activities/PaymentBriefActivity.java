package com.remouldwala.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.remouldwala.R;
import com.remouldwala.adapters.PaymentAdapter;
import com.remouldwala.models.TyreInfoModel;
import com.remouldwala.utils.ResUtil;
import com.remouldwala.utils.Utilities;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class PaymentBriefActivity extends AppCompatActivity implements PaymentAdapter.onUpdatePrice {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private ImageButton mIvAdd;
    private TextView mTvTyreSize;
    private TextView mTvNoOfTyre;
    private TextView mTvPrice;
    private TextView mTvTotal;
    private RecyclerView mRvTyreTotal;
    private TextView mTvGrandTotal;
    private TextView mTvTotalPrice;
    private LinearLayout mLlBottomLayout;
    private Button mBtnMakePayment;
    private Context mContext;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private PaymentAdapter paymentAdapter;
    private List<TyreInfoModel> tyreInfoModels;
    private float finalPrice;
    private NumberFormat format;
    private TextView mTvPattern;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_brief);
        mContext = this;

        initView();

        format = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("PAYMENT");

        linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        dividerItemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);

        mRvTyreTotal.setLayoutManager(linearLayoutManager);
        mRvTyreTotal.addItemDecoration(dividerItemDecoration);

        LinearLayout.LayoutParams params0 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        mTvPattern.setLayoutParams(params0);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        mTvTyreSize.setLayoutParams(params);

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        mTvNoOfTyre.setLayoutParams(params1);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        mTvPrice.setLayoutParams(params2);

        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        mTvTotal.setLayoutParams(params3);

        RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        params4.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        mTvGrandTotal.setLayoutParams(params4);

        RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(Utilities.getScreenWidth() / 3, ResUtil.getInstance().convertDpToPx(50));
        params5.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        mTvTotalPrice.setLayoutParams(params5);

        Intent intent = getIntent();

        if (intent != null) {
            String data = intent.getStringExtra("PaymentData");
            tyreInfoModels = new Gson().fromJson(data, new TypeToken<List<TyreInfoModel>>() {
            }.getType());

            paymentAdapter = new PaymentAdapter(tyreInfoModels, mContext, PaymentBriefActivity.this);
            mRvTyreTotal.setAdapter(paymentAdapter);
        }

    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mIvAdd = findViewById(R.id.iv_add);
        mTvTyreSize = findViewById(R.id.tv_tyre_size);
        mTvNoOfTyre = findViewById(R.id.tv_no_of_tyre);
        mTvPrice = findViewById(R.id.tv_price);
        mTvTotal = findViewById(R.id.tv_total);
        mRvTyreTotal = findViewById(R.id.rv_tyre_total);
        mTvGrandTotal = findViewById(R.id.tv_grand_total);
        mTvTotalPrice = findViewById(R.id.tv_total_price);
        mLlBottomLayout = findViewById(R.id.ll_bottom_layout);
        mBtnMakePayment = findViewById(R.id.btn_make_payment);
        mTvPattern = (TextView) findViewById(R.id.tv_pattern);
    }

    @Override
    public void onFinalPrice(float price) {
        finalPrice = finalPrice + price;
        mTvTotalPrice.setText(format.format(finalPrice));
    }
}

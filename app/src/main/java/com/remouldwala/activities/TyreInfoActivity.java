package com.remouldwala.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.remouldwala.R;
import com.remouldwala.adapters.TyrePatternAdapter;
import com.remouldwala.custom_components.OnSingleClickListener;
import com.remouldwala.models.TyreInfoModel;
import com.remouldwala.models.TyrePatternModel;
import com.remouldwala.utils.ResUtil;

import java.util.ArrayList;
import java.util.List;

public class TyreInfoActivity extends BaseActivity implements TyrePatternAdapter.OnPatterSelection {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private ImageButton mIvAdd;
    private TextView mTvSelectSize;
    private EditText mEtNoOfTyre;
    private Context mContext;
    private TyreInfoModel tyreInfoModel;
    private String[] sizesList = {"Select Size", "12.00.20", "11.00.20", "10.00.20", "9.00.20", "8.25.20", "8.25.16", "7.50.20", "7.50.16"};
    private TyrePatternAdapter tyrePatternAdapter;
    private GridLayoutManager gridLayoutManager;
    private TableRow mTrRadioGroup;
    private RecyclerView mRvPatternType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_info);
        mContext = this;

        initView();

        setSupportActionBar(mToolbar);

        tyreInfoModel = new TyreInfoModel();

        mTvPageTitle.setText("Enter Tyre Details");

        mIvBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        mIvAdd.setVisibility(View.VISIBLE);

        mIvAdd.setImageDrawable(ResUtil.getInstance().getDrawable(android.R.drawable.ic_delete));

        mIvAdd.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                if (TextUtils.isEmpty(tyreInfoModel.getSize())) {
                    Toast.makeText(mContext, "Please select a size", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(tyreInfoModel.getQuantity())) {
                    Toast.makeText(mContext, "Please enter no. of tyres", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(tyreInfoModel.getPattern())) {
                    Toast.makeText(mContext, "Please select a patter type", Toast.LENGTH_SHORT).show();
                    return;
                }

                String data = new Gson().toJson(tyreInfoModel);

                if (!TextUtils.isEmpty(data)) {
                    Intent intent = new Intent();
                    intent.putExtra("Data", data);
                    setResult(1001, intent);
                    finish();
                }
            }
        });

        gridLayoutManager = new GridLayoutManager(mContext, 2);

        tyrePatternAdapter = new TyrePatternAdapter(getPatternData(), mContext, this);
        mRvPatternType.setAdapter(tyrePatternAdapter);

        mTvSelectSize.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, R.layout.support_simple_spinner_dropdown_item, sizesList);

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                        if (which != 0) {

                            String strName = arrayAdapter.getItem(which);

                            mTvSelectSize.setText(strName);
                            tyreInfoModel.setSize(strName);

                        }

                        dialogInterface.dismiss();

                    }
                });

                builder.show();
            }
        });

        mEtNoOfTyre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!TextUtils.isEmpty(charSequence)) {
                    tyreInfoModel.setQuantity(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private List<TyrePatternModel> getPatternData() {
        List<TyrePatternModel> patternModels = new ArrayList<>();
        patternModels.add(new TyrePatternModel("4500", "Rib Nylon"));
        patternModels.add(new TyrePatternModel("4500", "Lug Nylon"));
        patternModels.add(new TyrePatternModel("4500", "Rib Radial"));
        patternModels.add(new TyrePatternModel("4500", "Lug Radial"));
        return patternModels;
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mIvAdd = findViewById(R.id.iv_add);
        mTvSelectSize = findViewById(R.id.tv_select_size);
        mEtNoOfTyre = findViewById(R.id.et_no_of_tyre);
        mTrRadioGroup = findViewById(R.id.tr_radio_group);
        mRvPatternType = findViewById(R.id.rv_pattern_type);
    }

    @Override
    public void onSelectedPattern(String selectedPattern, String price) {
        tyreInfoModel.setPattern(selectedPattern);
        tyreInfoModel.setPrice(price);
    }
}

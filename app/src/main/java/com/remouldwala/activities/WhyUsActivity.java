package com.remouldwala.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.remouldwala.R;
import com.remouldwala.adapters.IntroPagerAdapter;
import com.remouldwala.custom_components.OnSingleClickListener;
import com.remouldwala.models.IntroModel;
import com.remouldwala.utils.ResUtil;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class WhyUsActivity extends BaseActivity {

    private Toolbar mToolbar;
    private TextView mTvPageTitle;
    private ImageButton mIvBack;
    private RelativeLayout mBottomLayout;
    private TextView mBtnSkip;
    private CircleIndicator mCiIntroIndicator;
    private TextView mBtnNext;
    private Context mContext;
    private int[] introColors = {R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorPrimary, R.color.colorAccent};
    private ViewPager mViewpager;
    private IntroPagerAdapter introPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_why_us);
        mContext = this;
        initView();

        setToolbar();

        introPagerAdapter = new IntroPagerAdapter(getData(), mContext);
        mViewpager.setAdapter(introPagerAdapter);
        mCiIntroIndicator.setViewPager(mViewpager);

        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                onPageChanged(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBtnNext.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                if (mViewpager.getCurrentItem() < getData().size() - 1) {
                    mViewpager.setCurrentItem(mViewpager.getCurrentItem() + 1, true);
                } else {
                    if (mViewpager.getCurrentItem() == getData().size() - 1) {
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });


        mBtnSkip.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void onPageChanged(int position) {
        mBottomLayout.setBackgroundColor(ResUtil.getInstance().getColor(introColors[position]));


    }

    private List<IntroModel> getData() {
        List<IntroModel> introModels = new ArrayList<>();
        //    public IntroModel(String title, String description, String thumbnail, int back_color) {
        introModels.add(new IntroModel(1, "Save Huge", "Retread with us and save upto 70% on new tyre cost.", "https://placeimg.com/640/480/tech", R.color.colorAccent));
        introModels.add(new IntroModel(2, "Best Material", "Our Retreading Partner uses only premium Quality Tread & Retreading Material.", "https://placeimg.com/640/480/animals", R.color.colorPrimary));
        introModels.add(new IntroModel(3, "Hassle Free Solution", "No question asked claim policy.", "https://placeimg.com/640/480/arch", R.color.colorPrimaryDark));
        introModels.add(new IntroModel(4, "No Extra Charges", "No repairing charges.", "https://placeimg.com/640/480/nature", R.color.colorPrimary));
        introModels.add(new IntroModel(5, "Don't go anywhere", "Service at your doorstep.", "https://placeimg.com/640/480/people", R.color.colorAccent));

        return introModels;
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("Why RemouldWala");
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mIvBack = findViewById(R.id.iv_back);
        mBottomLayout = findViewById(R.id.bottom_layout);
        mBtnSkip = findViewById(R.id.btn_skip);
        mCiIntroIndicator = findViewById(R.id.ci_intro_indicator);
        mBtnNext = findViewById(R.id.btn_next);
        mViewpager = findViewById(R.id.viewpager);
    }
}

package com.remouldwala.application;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.remouldwala.utils.ResUtil;
import com.remouldwala.utils.SharedPrefs;

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Initiating Stetho for database data visible
        Stetho.initializeWithDefaults(this);

        //Initiating Resource Utils
        ResUtil.init(this);

        //Initiating SharedPreference
        SharedPrefs.init(this);
    }
}

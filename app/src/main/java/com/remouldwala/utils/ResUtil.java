package com.remouldwala.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

public class ResUtil {

    private static ResUtil mInstance;

    private Context mContext;

    private ResUtil(Context context) {
        mContext = context;
    }

    public static void init(Context context) {
        mInstance = new ResUtil(context.getApplicationContext());
    }

    public static ResUtil getInstance() {
        return mInstance;
    }

    public String getString(@StringRes int id) {
        return mContext.getResources().getString(id);
    }


    public int getColor(@ColorRes int id) {
        return mContext.getResources().getColor(id);
    }

    public Drawable getDrawable(@DrawableRes int id) {
        return mContext.getResources().getDrawable(id);

    }

    public int convertDpToPx(int dp) {
        return Math.round(dp * (mContext.getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
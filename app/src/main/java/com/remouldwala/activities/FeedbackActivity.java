package com.remouldwala.activities;

import android.content.Context;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.remouldwala.R;

public class FeedbackActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private ImageButton mIvAdd;
    private TextView mTxtDriverBehvior;
    private RatingBar mRbVehicleNo;
    private TextView mTxtRetResponse;
    private RatingBar mRbRetResponse;
    private TextView mTxtProQuality;
    private RatingBar mRbProQuality;
    private TextView mTxtTimeTaken;
    private RatingBar mRbTimeTaken;
    private EditText mEtAdditionalComment;
    private MaterialButton mBtnConfirm;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mContext = this;

        initView();

        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("Feedback");

        mRbProQuality.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Toast.makeText(mContext, "" + v, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mIvAdd = findViewById(R.id.iv_add);
        mTxtDriverBehvior = findViewById(R.id.txt_driver_behvior);
        mRbVehicleNo = findViewById(R.id.rb_vehicle_no);
        mTxtRetResponse = findViewById(R.id.txt_ret_response);
        mRbRetResponse = findViewById(R.id.rb_ret_response);
        mTxtProQuality = findViewById(R.id.txt_pro_quality);
        mRbProQuality = findViewById(R.id.rb_pro_quality);
        mTxtTimeTaken = findViewById(R.id.txt_time_taken);
        mRbTimeTaken = findViewById(R.id.rb_time_taken);
        mEtAdditionalComment = findViewById(R.id.et_additional_comment);
        mBtnConfirm = findViewById(R.id.btn_confirm);
    }
}

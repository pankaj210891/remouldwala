package com.remouldwala.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.remouldwala.R;
import com.remouldwala.adapters.TyreSelectingAdapter;
import com.remouldwala.custom_components.OnSingleClickListener;
import com.remouldwala.models.TyreInfoModel;

import java.util.ArrayList;
import java.util.List;

public class TyreSelectingActivity extends BaseActivity {

    private Toolbar mToolbar;
    private ImageButton mIvBack;
    private TextView mTvPageTitle;
    private MaterialButton mBtnSubmit;
    private Context mContext;
    private ImageButton mIvAdd;
    private TyreSelectingAdapter tyreSelectingAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView mRvTyreList;
    private List<TyreInfoModel> tyreInfoModels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tyre_selecting);
        mContext = this;

        initView();

        setSupportActionBar(mToolbar);

        mTvPageTitle.setText("Enter Tyre Details");

        mIvAdd.setVisibility(View.VISIBLE);

        mIvBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        mIvAdd.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                startActivityForResult(new Intent(mContext, TyreInfoActivity.class), 1001);

            }
        });

        tyreInfoModels = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);

        mRvTyreList.setLayoutManager(linearLayoutManager);
        mRvTyreList.addItemDecoration(itemDecoration);

        mBtnSubmit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (tyreInfoModels.size() != 0) {
                    String data = new Gson().toJson(tyreInfoModels);
                    startActivity(new Intent(mContext, PaymentBriefActivity.class).putExtra("PaymentData", data));
                } else {
                    Toast.makeText(mContext, "Please add some tyre data", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mIvBack = findViewById(R.id.iv_back);
        mTvPageTitle = findViewById(R.id.tv_page_title);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mIvAdd = findViewById(R.id.iv_add);
        mRvTyreList = findViewById(R.id.rv_tyre_list);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1001: {

                if (data != null) {
                    TyreInfoModel tyreInfoModel = new Gson().fromJson(data.getStringExtra("Data"), TyreInfoModel.class);
                    tyreInfoModels.add(tyreInfoModel);
                    tyreSelectingAdapter = new TyreSelectingAdapter(tyreInfoModels, mContext);
                    mRvTyreList.setAdapter(tyreSelectingAdapter);
                }
                break;
            }
        }
    }
}

package com.remouldwala.models;

public class TyrePatternModel {
    private boolean checked;
    private String price, pattern;

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public TyrePatternModel(String price, String pattern) {
        this.price = price;
        this.pattern = pattern;
    }
}
